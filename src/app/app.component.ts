import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  numberOfRowsArr: any;

  totalCalculationFormGroup = new FormGroup({
    rowOne: new FormControl(''),
  });

  onChangeNumberOfRows(numberOfRows:any) {
    this.numberOfRowsArr = Array(+numberOfRows.target.value)
      .fill(0)
      .map((x, i) => i);
    console.log(this.numberOfRowsArr.length);
  }
}
