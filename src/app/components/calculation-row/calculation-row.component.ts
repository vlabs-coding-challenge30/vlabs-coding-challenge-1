import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'calculation-row',
  templateUrl: './calculation-row.component.html',
  styleUrls: ['./calculation-row.component.scss'],
})
export class CalculationRowComponent implements OnInit {
  multiplierFormGroup = new FormGroup({
    firstValue: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
  });

  totalValue = '';

  constructor() {}

  ngOnInit() {
    this.multiplierFormGroup.valueChanges.subscribe((values: { firstValue: number; }) => {
      if (values.firstValue) {
        this.totalValue = String(
          values.firstValue * values.firstValue * values.firstValue
        );
      }
    });
  }
}
